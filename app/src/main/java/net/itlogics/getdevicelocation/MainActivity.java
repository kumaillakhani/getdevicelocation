package net.itlogics.getdevicelocation;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Context mContext = MainActivity.this;
    Button btnGetLocation;
    GPSTracker gps;
    double currentLatitude, currentLongitude;

/**
 * @author: Kumail Raza Lakhani
 * For requesting Location permission in Android 6.0 or above.
 */
    private static final String[] INITIAL_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final String[] LOCATION_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int INITIAL_REQUEST=1337;
    private static final int LOCATION_REQUEST=INITIAL_REQUEST+1;
    int REQUEST_LOCATION;

/*
 * End's --->
 */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnGetLocation = (Button) findViewById(R.id.btnGetLocation);


/**
 *  Request Location permission in Android 6.0 or above
 */
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (!canAccessLocation()) {
                RequestPopup();
            } else {
                GetDeviceLocation();
            }
        } else {
            GetDeviceLocation();
        }

        btnGetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetDeviceLocation();
            }
        });
    }

/**
 *  Popup Location permission in Android 6.0 or above.
 */
    public void RequestPopup() {
        AlertDialog.Builder builder =  new  AlertDialog.Builder(this);
        builder.setTitle("Location Permission")
                .setMessage("Location access is required to use this app.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
//                                    requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
                        requestPermissions(new String[]{
                                        Manifest.permission.ACCESS_FINE_LOCATION,
                                        Manifest.permission.ACCESS_COARSE_LOCATION},
                                REQUEST_LOCATION);
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }
/**
 *  Popup Location permission in Android 6.0 or above. END ->
 */


/**
 *  Request Location permission in Android 6.0 or above.
 */
    private boolean canAccessLocation() {
        return(hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED== ContextCompat.checkSelfPermission(this, perm));
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if(grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // We can now safely use the API we requested access to
                GetDeviceLocation();
            } else {
                // Permission was denied or request was cancelled
                RequestPopup();
            }
        }
    }
/**
 *  Request Location permission in Android 6.0 or above. END ->
 */

/**
 *  Get device location.
 */
    public void GetDeviceLocation() {
        try {
            gps = new GPSTracker(mContext);
            if (gps.canGetLocation()) {

                currentLatitude = gps.getLatitude(); // returns latitude
                currentLongitude = gps.getLongitude(); // returns longitude

                ((TextView) findViewById(R.id.tvLocation)).setText(currentLatitude + ", " + currentLongitude);

//                String uri = "http://maps.google.com/maps?f=d&hl=en&saddr=" + currentLatitude + "," + currentLongitude + "&daddr=" + obj.branchLatitude + "," + obj.branchLongitude;
//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
//                startActivity(Intent.createChooser(intent, "Select an application"));
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/Network in settings
                gps.showSettingsAlert();
            }
        } catch (Exception e) {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/Network in settings
            gps.showSettingsAlert();
            Toast.makeText(mContext, "Error", Toast.LENGTH_LONG);
        }
    }
/**
 *  Get device End.
 */

}
